import { Html, Head, Main, NextScript } from "next/document";
import HeaderPage from "./component/header";

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <HeaderPage />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
