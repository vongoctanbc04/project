import React from 'react';
import YouTube from 'react-youtube';
import Image from "next/image"
import re1 from "../Image/re1.jpeg"
import re2 from "../Image/rs2.svg"
import kh1 from "../Image/kh1.jpeg"
import kh2 from "../Image/kh2.jpeg"
import kh3 from "../Image/kh3.jpeg"
import imageReal from "../Image/imagereal.jpeg"
import call from "../Image/phone-call.svg"
import logo1 from "../Image/th1.png"
import logo2 from "../Image/th2.png"
import logo3 from "../Image/th3.png"
import logo4 from "../Image/th4.png"
import logo5 from "../Image/th5.png"
import logo6 from "../Image/hi.svg"
import Section3ladipage from '../ladipage/section3ladipage';

const Page = () => {
    const opts = {
        width: '100%',
        height: '421',
        playerVars: {
            autoplay: 1,
        },
    };
    const videoId = "PDPo2zEDbLM"

    const tan = 'border-4 border-cyan-600 rounded-full overflow-hidden w-[66%] h-[200px] m-auto'


    return <div className="">
        <div className="h-[700px]" style={{ backgroundImage: "url(https://w.ladicdn.com/s1440x800/uploads/images/9b7e3a84-fe58-484c-a396-d71df080205e.jpg)" }}>
            <div className="text-4xl font-bold text-sky-400 text-center pt-24">CĂN HỘ CAO CẤP HOT NHẤT 2017</div>
            <div className="text-xl text-white text-center pt-4 w-[40%] m-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. De illis, cum volemus. Quare conare, quaeso. Quid sequatur, quid repugnet, viden</div>
            <div className="flex gap-6 justify-center pt-12 w-[55%] m-auto">
                <div className="w-[595px] h-[344px] border-2 border-white"><Image src={re1} alt="re1" className="h-[100%]" /></div>
                <div className="w-[42%]">
                    <div className="text-2xl font-bold text-sky-400">Quare conare, quaeso. Quid sequatur, quid repugnet, viden.</div>
                    <div className="pt-12">
                        <div className="flex gap-4">
                            <Image src={re2} alt="re2" />
                            <div className="text-white font-light">Publish conversion-optimized landing pages</div>
                        </div>
                        <div className="flex gap-4">
                            <Image src={re2} alt="re2" />
                            <div className="text-white font-light">Build your mailing list</div>
                        </div>
                        <div className="flex gap-4">
                            <Image src={re2} alt="re2" />
                            <div className="text-white font-light">Create awesome marketing pages & content</div>
                        </div>
                    </div>
                    <div className="pt-12">
                        <button className="text-white bg-sky-400 px-12 py-2 rounded-full">Tìm hiểu thêm</button>
                    </div>
                </div>
            </div>
        </div>
        <div className='max-w-screen-xl m-auto pt-12'>
            <div>
                <div className='text-4xl text-center pb-8'>Siêu đô thị xanh giữa lòng Hà Nội</div>
                <div className='pb-6 w-[60%] m-auto text-center text-slate-600'>Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm.</div>
                <div>
                    <YouTube videoId={videoId} opts={opts} />
                </div>
            </div>
        </div>
        <div className='mt-12 bg-cyan-500 pb-12'>
            <div className='max-w-screen-xl m-auto'>
                <div className='pt-10 text-center text-4xl font-medium text-white'>LDP Building</div>
                <div className='text-center text-white pt-4 w-[60%] m-auto pb-12'>Mô tả điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm/dịch vụ. Hoặc mô tả điểm đặc biệt của từng thành phần cấu thành sản phẩm</div>
                <div className='flex justify-between gap-10'>
                    <div>
                        <Image src={kh1} alt='kh1' className='w-[100%] h-[220px]' />
                        <div className='bg-white'>
                            <div className='text-xl text-cyan-500 font-medium text-center pt-6 pb-2'>VỊ TRÍ ĐẮC ĐỊA</div>
                            <div className='w-[88%] m-auto text-center'>Mô tả điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm/dịch vụ. Hoặc mô tả điểm đặc biệt của từng thành phần cấu thành sản phẩm </div>
                        </div>
                    </div>
                    <div>
                        <Image src={kh2} alt='kh1' className='w-[100%] h-[220px]' />
                        <div className='bg-white'>
                            <div className='text-xl text-cyan-500 font-medium text-center pt-6 pb-2'>TÍCH HỢP HOÀN HẢO</div>
                            <div className='w-[88%] m-auto text-center'>Mô tả điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm/dịch vụ. Hoặc mô tả điểm đặc biệt của từng thành phần cấu thành sản phẩm </div>
                        </div>
                    </div>
                    <div>
                        <Image src={kh3} alt='kh1' className='w-[100%] h-[220px]' />
                        <div className='bg-white'>
                            <div className='text-xl text-cyan-500 font-medium text-center pt-6 pb-2'>NGÔI SAO BIỂN XANH</div>
                            <div className='w-[88%] m-auto text-center'>Mô tả điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm/dịch vụ. Hoặc mô tả điểm đặc biệt của từng thành phần cấu thành sản phẩm </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className='max-w-screen-xl m-auto pt-12'>
            <div className='text-center text-4xl font-medium pb-12'>Dịch vụ và tiện ích</div>
            <div className='flex gap-4'>
                <div>
                    <div className={tan}> <Image src={kh2} alt='kh2' className='h-[100%] object-cover' /></div>
                    <div className='font-medium text-cyan-400 pt-4 text-center'>KHÔNG GIAN THƯ GIÃN</div>
                    <div className='text-center w-[80%] m-auto'>Mô tả thông tin có giá trị, những điểm nổi bật về sản phẩm </div>
                    <div className='bg-red-600 h-1 w-[30%] m-auto mt-2'></div>
                </div>
                <div>
                    <div className='border-4 border-cyan-600 rounded-full overflow-hidden w-[66%] h-[200px] m-auto'> <Image src={kh2} alt='kh2' className='h-[100%] object-cover' /></div>
                    <div className='font-medium text-cyan-400 pt-4 text-center'>KHÔNG GIAN THƯ GIÃN</div>
                    <div className='text-center w-[80%] m-auto'>Mô tả thông tin có giá trị, những điểm nổi bật về sản phẩm </div>
                    <div className='bg-red-600 h-1 w-[30%] m-auto mt-2'></div>
                </div>
                <div>
                    <div className='border-4 border-cyan-600 rounded-full overflow-hidden w-[66%] h-[200px] m-auto'> <Image src={kh2} alt='kh2' className='h-[100%] object-cover' /></div>
                    <div className='font-medium text-cyan-400 pt-4 text-center'>KHÔNG GIAN THƯ GIÃN</div>
                    <div className='text-center w-[80%] m-auto'>Mô tả thông tin có giá trị, những điểm nổi bật về sản phẩm </div>
                    <div className='bg-red-600 h-1 w-[30%] m-auto mt-2' ></div>
                </div>
                <div>
                    <div className='border-4 border-cyan-600 rounded-full overflow-hidden w-[66%] h-[200px] m-auto'> <Image src={kh2} alt='kh2' className='h-[100%] object-cover' /></div>
                    <div className='font-medium text-cyan-400 pt-4 text-center'>KHÔNG GIAN THƯ GIÃN</div>
                    <div className='text-center w-[80%] m-auto mt-2'>Mô tả thông tin có giá trị, những điểm nổi bật về sản phẩm </div>
                    <div className='bg-red-600 h-1 w-[30%] m-auto'></div>
                </div>
            </div>
        </div>
        <div className='bg-slate-300 h-1 mt-16 mb-16'></div>
        <div className='max-w-screen-xl m-auto py-12'>
            <div className='grid grid-cols-3 gap-4'>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>
                <div>
                    <Image src={kh3} alt='kh3' />
                </div>

            </div>
        </div>
        <div className='bg-cyan-500'>
            <div className='max-w-screen-xl m-auto text-white flex justify-between py-12'>
                <div>
                    <div className='text-3xl font-serif'>Bạn cần chúng tôi giúp đỡ?</div>
                    <div className='pt-4'>Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ</div>
                </div>
                <div>
                    <div className='text-3xl font-serif'>Hỗ trợ khách hàng 24/24</div>
                    <div className='flex pt-4'>
                        <Image src={call} alt='call' />
                        <div className=' pl-4'>0972 220 777</div>
                    </div>
                </div>
                <button className='bg-white px-6 py-2 text-black rounded-md'>LIÊN HỆ</button>
            </div>
        </div>
        <div className='max-w-screen-xl m-auto flex gap-5 pt-24'>
            <div className='w-[50%]'>
                <div className='text-4xl font-mono font-bold'>Vị trí địa lý đắc địa</div>
                <div className='pt-4 text-2xl pb-12'>Mô tả chung về dự án, sản phẩm, dịch vụ của bạn</div>
                <div className='bg-cyan-400 h-1 w-[20%] mb-12'></div>
                <div className='pt-6'>Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm. Những mô tả được viết tốt kèm theo các từ khóa phù hợp có thể tăng lượt xem vì chúng giúp landingpage của bạn hiển thị tốt trong kết quả tìm kiếm.
                </div>
                <div className='pt-6'>Mô tả thông tin có giá trị, những điểm nổi bật, lời giới thiệu hấp dẫn về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm. Những mô tả được viết tốt kèm theo các từ khóa phù hợp có thể tăng lượt xem vì chúng giúp landingpage của bạn hiển thị tốt trong kết quả tìm kiếm.
                </div>
            </div>
            <div>
                <Image src={imageReal} alt='imageReal' className='h-[500px]' />
            </div>
        </div>
        <div className='bg-slate-300'>
            <div className='max-w-screen-xl m-auto'>
                <div className='pt-12 text-center text-4xl font-normal'>ĐỐI TÁC ĐỒNG HÀNH</div>
                <div className=' py-6 text-center'>Những đối tác vàng, nhà tài trợ và các đơn vị bảo trợ truyền thông lớn đã đồng hành cùng chúng tôi tổ chức chương trình này.</div>
                <div className='flex justify-between'>
                    <Image src={logo1} alt="logo1" />
                    <Image src={logo2} alt="logo1" />
                    <Image src={logo3} alt="logo1" />
                    <Image src={logo4} alt="logo1" />
                    <Image src={logo5} alt="logo1" />
                </div>
            </div>
        </div>
        <div className='max-w-screen-md m-auto pt-12'>
            <div className='text-3xl text-center pb-2'>Đăng ký miễn phí</div>
            <div className='text-center pb-4'>Hãy để lại thông tin liên hệ của bạn để nhận thông tin miễn phí từ chúng tôi</div>
            <div>
                <div className='flex gap-3'>
                    <input type="name" id="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" placeholder="Họ và Tên" required />
                    <input type="name" id="sdt" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" placeholder="Nhập số điện thoại" required />
                </div>
                <div className='pt-6'>
                    <input type="name" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" placeholder="Nhập Eamil" required />
                </div>
            </div>
            <div className='flex justify-center pt-6'>
                <button className='bg-cyan-400 text-white px-6 py-2 rounded-lg'>Gửi đi</button>
            </div>
            <div className='text-center'>Chuyên viên của chúng tôi sẽ liên hệ sớm với bạn</div>
        </div>
        <div className='bg-slate-300 h-1 mt-16 mb-16'></div>
        <Section3ladipage />
    </div>
}
export default Page