import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
  price: string;
  image: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const data: Data[] = [
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/0dcf15a1-248e-43a0-ac4e-abde5f535056.jpg",
    },
    {
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      price: "$10.00",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/75fe0f7b-c826-4e4f-830c-c5b6db8af208.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/adee7353-d519-4b05-88c4-1962944d4c4a.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/77e57f14-e75e-4a20-a453-6cdbe0a16931.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/ec10198e-48c8-4464-be55-9ab5ad2ee5b3.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/ba37c337-5e68-453d-8f04-933794dfce59.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/b45d3d86-aca6-4cb6-aaf1-473c7ecb5277.jpg",
    },
    {
      price: "$50.00",
      name: "Gang tay nhà bếp (màu xám) màu xám lông cừu",
      image:
        "https://w.ladicdn.com/s400x400/uploads/images/eb8a300e-718f-45f1-8d7f-f342008095ab.jpg",
    },
  ];

  res.status(200).json(data);
}
