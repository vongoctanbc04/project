import Image from "next/image";
import { useEffect, useState } from "react"
import gd1 from "../Image/gd1.jpeg"
import gd2 from "../Image/gd2.jpeg"
import sc2 from "../Image/hinhsection2.jpeg"
import project1 from "../Image/project1.jpeg"
import project2 from "../Image/project2.jpeg"
import logo1 from "../Image/th1.png"
import logo2 from "../Image/th2.png"
import logo3 from "../Image/th3.png"
import logo4 from "../Image/th4.png"
import logo5 from "../Image/th5.png"
import people1 from "../Image/people1.png"
import people2 from "../Image/people2.jpeg"
import people3 from "../Image/people3.jpeg"
import acmh from "../Image/acmh.png"


/* eslint-disable @next/next/no-img-element */
interface ProductData {
    name: string;
    price: string;
    image: string;
}

const Section2ladipage = () => {

    const [data, setDate] = useState<ProductData[]>([])

    useEffect(() => {
        const fetaData = async () => {
            const response = await fetch("http://localhost:3000/api/houseware")
            const jsonData = await response.json()
            setDate(jsonData)
        }
        fetaData()
    }, [])
    console.log("Tan", data)
    return <div>
        <div className="text-center pt-8 pb-16 text-4xl w-[16%] m-auto">Sản phẩm bán chạy
            <div className="h-[1px] bg-lime-400 w-[50%] mt-2 m-auto"></div>
        </div>

        <div className=" grid gap-x-4 gap-y-8 grid-cols-4 w-[70%] m-auto">
            {data!.map((index: ProductData) => {
                return <div key={index.name} className="w-[306px] h-[425px]">
                    <div className="text-center">
                        <img src={index.image} alt="hi" style={{
                            height: "360px"
                        }} ></img>
                        <div>{index.name}</div>
                        <div className="text-lime-400">Giá bán : {index.price}</div>
                    </div>
                </div>
            })}
        </div>
        <div className="bg-lime-600 mt-4 pt-16 pb-16 text-white">
            <div className="w-[30%] m-auto text-center"> Với hơn 20 năm trong ngành cung cấp đồ da dụng, chúng tôi đã mang lại sự đầy đủ và tiện nghi trong từng mái ấm gia đình Việt. Đó chính là sự tự hào cũng như tinh thần phục vụ khách hàng của chúng tôi!...</div>
            <div className="flex justify-center pt-2">
                <button className="border-2 border-white px-6 py-[3px] rounded-full mt-4">TÌM HIỂU NGAY</button>
            </div>
        </div>
        <div className="flex gap-9 justify-center items-center pt-16">
            <div className="relative">
                <Image src={gd1} alt="gd1" />
                <div className="w-[250px] absolute bottom-[6%] left-[6%] z-10 bg-white">
                    <div className="w-[82%] m-auto mt-6 mb-2">
                        <div>Dịch vụ cung cấp trọn gói từ A đến Z các mặt hàng đồ gia dụng.</div>
                        <div className="flex justify-between pt-2 pb-4">
                            <div className="font-bold">100$-200$</div>
                            <div className="text-lime-600">Sale 20%</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="relative">
                <Image src={gd2} alt="gd1" />
                <div className="w-[250px] absolute bottom-[6%] left-[6%] z-10 bg-white">
                    <div className="w-[82%] m-auto mt-6 mb-2">
                        <div>Dịch vụ cung cấp trọn gói từ A đến Z các mặt hàng đồ gia dụng.</div>
                        <div className="flex justify-between pt-2 pb-4">
                            <div className="font-bold">100$-200$</div>
                            <div className="text-lime-600">Sale 20%</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="relative">
                <Image src={gd1} alt="gd1" />
                <div className="w-[250px] absolute bottom-[6%] left-[6%] z-10 bg-white">
                    <div className="w-[82%] m-auto mt-6 mb-2">
                        <div>Dịch vụ cung cấp trọn gói từ A đến Z các mặt hàng đồ gia dụng.</div>
                        <div className="flex justify-between pt-2 pb-4">
                            <div className="font-bold">100$-200$</div>
                            <div className="text-lime-600">Sale 20%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className=" flex justify-center items-center pt-8 w-[70%] m-auto">
            <Image src={sc2} alt="sc2" className="w-[50%]" />
            <div className="">
                <div className="text-4xl font-seri pb-4">Giới thiệu dự án</div>
                <div>Mô tả thông tin có giá trị về sản phẩm dịch vụ, giúp người xem dễ dàng tìm thấy nội dung của bạn trong kết quả tìm kiếm. Những mô tả được viết kèm theo các từ khóa phù hợp có thể tăng lượt xem vì chúng giúp landingpage của bạn hiển thị tốt trong kết quả tìm kiếm.</div>
                <div className="text-2xl pt-2 pb-2">Các dự án đã tham gia phát triển</div>
                <div className="flex ">
                    <div className="w-[50%]">
                        <Image src={project1} alt="project1" className="h-[150px]" />
                        <div className="pt-2">Dự án 1</div>
                        <div>Mô tả dự án.</div>
                    </div>
                    <div className="w-[50%]">
                        <Image src={project2} alt="project2" className="h-[150px]" />
                        <div className="pt-2">Dự án 2</div>
                        <div>Mô tả dự án.</div>
                    </div>
                </div>
            </div>
        </div>
        <div className="bg-slate-300">
            <div className=" flex gap-16 justify-center items-center py-8">
                <Image src={logo1} alt="logo1" />
                <Image src={logo2} alt="logo1" />
                <Image src={logo3} alt="logo1" />
                <Image src={logo4} alt="logo1" />
                <Image src={logo5} alt="logo1" />
            </div>
        </div>
        <div className="py-16">
            <div className="text-center pb-12 text-4xl font-bold">KHÁCH HÀNG ĐÁNH GIÁ</div>
            <div className="w-[70%] m-auto flex gap-16">
                <div className=" w-[30%]">
                    <div className="rounded-full overflow-hidden w-[260px] h-[260px] m-auto">
                        <Image src={people1} alt="people1" className="w-[100%]" />
                    </div>
                    <div className="flex pt-4">
                        <div className="w-[45%]">
                            <Image src={acmh} alt="acmh" />
                        </div>
                        <div >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, repellat, voluptatem eveniet et illum itaque excepturi repudiandae officiis quibusdam deleniti.
                        </div>
                    </div>
                    <div className="text-center pt-2 text-sm text-slate-500">— Ashley Doe, CEO Abc Organisation</div>
                </div>
                <div className=" w-[30%]">
                    <div className="rounded-full overflow-hidden w-[260px] h-[260px] m-auto">
                        <Image src={people2} alt="people1" className="w-[100%]" />
                    </div>
                    <div className="flex pt-4">
                        <div className="w-[45%]">
                            <Image src={acmh} alt="acmh" />
                        </div>
                        <div >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, repellat, voluptatem eveniet et illum itaque excepturi repudiandae officiis quibusdam deleniti.
                        </div>
                    </div>
                    <div className="text-center pt-2 text-sm text-slate-500">— Ashley Doe, CEO Abc Organisation</div>
                </div>
                <div className=" w-[30%]">
                    <div className="rounded-full overflow-hidden w-[260px] h-[260px] m-auto">
                        <Image src={people3} alt="people1" className="w-[100%]" />
                    </div>
                    <div className="flex pt-4">
                        <div className="w-[45%]">
                            <Image src={acmh} alt="acmh" />
                        </div>
                        <div >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, repellat, voluptatem eveniet et illum itaque excepturi repudiandae officiis quibusdam deleniti.
                        </div>
                    </div>
                    <div className="text-center pt-2 text-sm text-slate-500">— Ashley Doe, CEO Abc Organisation</div>
                </div>
            </div>
        </div>
    </div>
}
export default Section2ladipage