import code from "../Image/home.svg"
import sdt from "../Image/phone-call.svg"
import emai from "../Image/message-square.svg"
import time from "../Image/pie-chart.svg"
import Image from "next/image"


const Section3ladipage = () => {
    return <div className=" pt-12">
        <div className="text-2xl font-semibold text-center">LIÊN HỆ</div>
        <div className="w-[30%] m-auto pt-2 pb-6 text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        <div className="flex w-[80%] m-auto ">
            <div className="w-[30%]">
                <div className="tracking-wider">ĐỊA CHỈ VĂN PHÒNG</div>
                <div className="pt-2 flex gap-3 mb-6" >
                    <Image src={code} alt="code" />
                    <div>Số 9A - Ngõ 9 - Hoàng Cầu - Hà Nội</div>
                </div>
                <div className="flex gap-3 mb-6">
                    <Image src={sdt} alt="code" />
                    <div>012.2345.567</div>
                </div>
                <div  className="flex gap-3" >
                    <Image src={emai} alt="emai" />
                    <div>support@ladipage.vn</div>
                </div>
            </div>
            <div className="w-[30%]">
                <div className="tracking-wider">THỜI GIAN LÀM VIỆC</div>
                <div className="pt-2 flex gap-3 mb-6">
                    <Image src={time} alt="time" />
                    <div>Thứ 2 - Thứ 6: 8AM đến 5PM</div>
                </div>
                <div className="flex gap-3 mb-6">
                    <Image src={time} alt="time" />
                    <div>Thứ 7: 8AM đến 11AM</div>
                </div>
                <div className="flex gap-3 mb-6">
                    <Image src={time} alt="time" />
                    <div>Chủ nhật: Nghỉ</div>
                </div>
            </div>
            <div className="w-[30%]">
                <div className="tracking-wider">LIÊN HỆ</div>
                <div className="pt-2">

                    <form>
                        <div className="mb-2">
                            <input type="name" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Họ và Tên" required />
                        </div>
                        <div className="mb-2">
                            <input type="email" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nhập Email" required />
                        </div>
                        <div className="mb-2">
                            <input type="sdt" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nhập Số điện thoại" required />
                        </div>
                        <button type="submit" className="text-white bg-green-500 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Gửi đi</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
}
export default Section3ladipage