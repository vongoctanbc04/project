import Image from "next/image"
import logo from "../Image/codepen.svg"
import may from "../Image/may-sinh-to.png"

const LadipageSection1 = () => {
    return <div className="min-h-screen block bg-cover bg-no-repeat bg-top " style={{ backgroundImage: "url(https://static.ladipage.net/s1440x900/uploads/images/a4e64573-f576-43e7-9f55-f6f77abbcfa4.jpg)" }}>
        <div className="min-h-screen bg-green-500 bg-opacity-50 flex flex-col justify-center items-center text-white">
            <div>
                <Image src={logo} alt="logo" />
            </div>
            <div className="text-3xl pt-4 pb-2">Đồ gia dụng LDP</div>
            <div>Lorem ipsum dolor sit amet, consetetur sadipscing elitr diam nonumyeirmod tempor invidunt ut</div>
            <div className="border-2 border-white px-6 py-[3px] rounded-full mt-4">
                <button>SỬ DỤNG !</button>
            </div>
            <div className="pt-4">
                <Image src={may} alt="may"/>
            </div>
        </div>
    </div>
}
export default LadipageSection1
// min-h-screen