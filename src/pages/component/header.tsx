import React from "react";
import logo from "../Image/logo.png"
import Link from "next/link";
import Image from "next/image";

const HeaderPage = () => {
    return <div className="bg-stone-700 w-full py-6 sticky top-0 ">
        <div className="flex w-[95%] m-auto justify-between ">
            <Link href="/"><Image src={logo} alt="logo" /></Link>
            <div className="flex gap-12 text-amber-500 pt-2">
                <Link href="/realEstate">REAL ESTATE</Link>
                <Link href="/music">LADIPAGE MUSIC</Link>
                <div>LADIPAGE FLOWERS</div>
            </div>
            <button className="rounded-full bg-red-400 px-4 animate-scale " >ĐĂNG NHẬP</button>
            {/* <button type="button" className="text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-4 focus:ring-yellow-300 font-medium rounded-full text-sm px-5 py-2.5 text-center me-2 mb-2 dark:focus:ring-yellow-900">ĐĂNG NHẬP</button> */}
        </div>

    </div>;
}
export default HeaderPage