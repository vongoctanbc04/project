import Image from "next/image";
import { Inter } from "next/font/google";
import LadipageSection1 from "./ladipage/section1ladipage";
import Section2ladipage from "./ladipage/section2ladipage";
import Section3ladipage from "./ladipage/section3ladipage";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <div>
      <LadipageSection1 />
      <Section2ladipage />
      <Section3ladipage />
    </div>
  );
}
